
<?php
    // User login will be redirected to the home page
    if(isset($_SESSION['USER'])){
        header('Location: '.'index.php');
    }
    //Get data from the form put into the session
    $valuePost = $_POST;
    $_SESSION['valuePost'] = $valuePost;
    $arrError = [];
    unset($_SESSION['alert_message_error']);
    unset($_SESSION['alert_message_success']);

    if(isset($valuePost['submit'])){
        //validation data
        if (isset($valuePost['first_name']) && $valuePost['first_name'] === '') {
            $arrError["first_name_required"] = msg_required;
        }
    
        if (isset($valuePost['second_name']) && $valuePost['second_name'] === '') {
            $arrError["second_name_required"] = msg_required;
        }
    
        if (isset($valuePost['email']) && $valuePost['email'] === '') {
            $arrError["email_required"] = msg_required;
        } else if (isset($valuePost['email']) && !filter_var($valuePost['email'], FILTER_VALIDATE_EMAIL)) {
            $arrError["email_required"] = msg_email;
        }
    
        if (isset($valuePost['password']) && $valuePost['password'] === '') {
            $arrError["password_required"] = msg_required;
        } else {
            $pattern = '/^(?=.{6,}$)[A-Z].*[!^&].*[0-9]+$/';
            if(!preg_match($pattern, $valuePost['password'])) {
                $arrError["password_required"] =  msg_error_password;
            }
        }
    
        if (isset($valuePost['referral']) && $valuePost['referral'] === '') {
            $arrError["referral_required"] = msg_required;
        }
    
        if (isset($valuePost['age']) && $valuePost['age'] === '') {
            $arrError["age_required"] = msg_required;
        }
    
        if (isset($valuePost['member_type']) && $valuePost['member_type'] === '') {
            $arrError["member_type_required"] = msg_required;
        }
    
        if (isset($valuePost['member_duaration']) && $valuePost['member_duaration'] === '') {
            $arrError["member_duaration_required"] = msg_required;
        }
    
        if (isset($valuePost['address']) && $valuePost['address'] === '') {
            $arrError["address_required"] = msg_required;
        }
    
        if (isset($valuePost['member_type']) && $valuePost['member_type'] === 'Family') {
            foreach($valuePost['member_child'] as $key => $value) {
                if ($value['fist_name'] === '') {
                    $arrError[$key]["first_name_required"] = msg_required;
                }
                if ($value['second_name'] === '') {
                    $arrError[$key]["second_name_required"] = msg_required;
                }
            }
        }
    
        //Open data file users.json
        $arrRedRecord = [];
        $fh = fopen(url_data_users,'r');
        $arrRedRecord = json_decode(fgets($fh));
        fclose($fh);
    
        //Check Duplicate user
        $isCheckDuplicate = false;
        if (!is_null($arrRedRecord) && isset($valuePost['email']) && $valuePost['email'] !== '') {
            foreach($arrRedRecord  as $key => $value) {
                if ($value->email == $valuePost['email']) {
                    $isCheckDuplicate = true;
                    break;
                }
            }
        }
    
        if ($isCheckDuplicate) {
            $arrError["email_required"] = msg_email_exits;
        }

        //Add info user to users.json
        if (count($arrError) == 0 && $isCheckDuplicate === false && isset($valuePost['submit'])) {
            $fp = fopen(url_data_users, 'w');
            unset($valuePost['submit']);
            $arrRedRecord[] = $valuePost;
            fwrite($fp, json_encode($arrRedRecord));
            fclose($fp);
            //$_SESSION['alert_message_success'] = msg_register_success;            
            $isLogin = false;
   
            //Open data file users.json
            $arrRedRecordNew = [];
            $fh = fopen(url_data_users,'r');
            $arrRedRecordNew = json_decode(fgets($fh));
            fclose($fh);

            foreach($arrRedRecordNew  as $key => $value) {
                if ($value->email == $valuePost['email'] && $value->password == $valuePost['password']) {
                    $isLogin = true;
                    $value->login_success = 1;
                    $_SESSION['USER'] = $value;
                    break;
                }
            }
            if(!$isLogin) {
                $_SESSION['alert_message_error'] = msg_login_error;
            } else {
                unset($_SESSION['valuePost']);
                header('Location: '.url_myfitness);
                exit();
            }
        }
    }

?>

<div class="container register">
    <h1 class="title">REGISTRATION FORM</h1>
    <form class="login marginTopForm registerForm"  method="post" action="index.php?page=register" onchange="mountTotals()">
        <?php require 'alert-message.php'; ?>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="inputEmail4" name="first_name" value="<?php echo isset($_SESSION['valuePost']['first_name']) ? $_SESSION['valuePost']['first_name'] : ''  ?>">
                <label  class="error"><?php echo isset($arrError["first_name_required"]) ? $arrError["first_name_required"] : ''  ?></label>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Second Name</label>
                <input type="text" class="form-control" id="inputPassword4" name="second_name" value="<?php echo isset($_SESSION['valuePost']['second_name']) ? $_SESSION['valuePost']['second_name'] : ''  ?>">
                <label  class="error"><?php echo isset($arrError["second_name_required"]) ? $arrError["second_name_required"] : ''  ?></label>
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Email</label>
            <input type="text" class="form-control" id="inputAddress" name="email" value="<?php echo isset($_SESSION['valuePost']['email']) ? $_SESSION['valuePost']['email'] : ''  ?>">
            <label  class="error"><?php echo isset($arrError["email_required"]) ? $arrError["email_required"] : ''  ?></label>
        </div>
         <div class="form-group">
            <label for="inputAddress">Password</label>
            <input type="password" class="form-control" id="inputAddress" name="password" value="<?php echo isset($_SESSION['valuePost']['password']) ? $_SESSION['valuePost']['password'] : ''  ?>">
            <label  class="error"><?php echo isset($arrError["password_required"]) ? $arrError["password_required"] : ''  ?></label>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label class="form-check-label" for="gridRadios1">
                    Referral
                </label>
            </div>
            <div class="form-group col-md-4">
                <input class="form-check-input" type="radio" name="referral" value="Yes" <?php echo isset($_SESSION['valuePost']['referral']) && $_SESSION['valuePost']['referral'] == 'Yes'  ? 'checked' : 'checked'  ?> >
                <label class="form-check-label" for="gridRadios1">
                    Yes
                </label>
            </div>
            <div class="form-group col-md-4">
                <input class="form-check-input" type="radio" name="referral" value="No" <?php echo isset($_SESSION['valuePost']['referral']) && $_SESSION['valuePost']['referral'] == 'No'  ? 'checked' : ''  ?> >
                <label class="form-check-label" for="gridRadios2">
                    No
                </label>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label class="form-check-label" for="gridRadios1">
                    Age
                </label>
            </div>
            <div class="form-group col-md-5">
                <input type="range" class="form-control-range" id="range_slider_1" name="age"  min="1" max="100"  value="<?php echo isset($_SESSION['valuePost']['age']) ? $_SESSION['valuePost']['age'] : 16  ?>" oninput="setRangeValue(1)">
            </div>
            <div class="form-group col-md-5">
                <label class="form-check-label">
                    Value : <span id="range_value_1"><?php echo isset($_SESSION['valuePost']['age']) ? $_SESSION['valuePost']['age'] : 16  ?></span>
                </label>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Membership Type</label>
                <select class="form-control" name="member_type" id="member_type">
                    <?php foreach(member_type as $k => $val){ ?>
                        <option value="<?php echo $k ?>" <?php echo isset($_SESSION['valuePost']['member_type']) && $_SESSION['valuePost']['member_type'] == $k  ? 'selected' : '' ?> ><?php echo $val ?></option>
                    <?php } ?>
                </select>
                <label  class="error"><?php echo isset($arrError["member_type_required"]) ? $arrError["member_type_required"] : ''  ?></label>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Duaration Of Membership</label>
                <select class="form-control" id="member_duaration" name="member_duaration">
                    <?php foreach(member_duaration as $key => $value){ ?>
                        <option value="<?php echo $key ?>" <?php echo isset($_SESSION['valuePost']['member_duaration']) && $_SESSION['valuePost']['member_duaration'] == $key  ? 'selected' : '' ?> ><?php echo $value ?></option>
                    <?php } ?>
                </select>
                <label  class="error"><?php echo isset($arrError["member_duaration_required"]) ? $arrError["member_duaration_required"] : ''  ?></label>
            </div>
            <?php if(isset($_SESSION['valuePost']['member_type']) && $_SESSION['valuePost']['member_type'] == 'Family'){ ?>
                <div class="col-md-12">
                        <div id="add_member" class="form-wrapper form-group">
                            <button type="button" onclick="OnlickAddButton()" class="btn btn-primary"> + Add Member</button>
                            <span>Member toals: </span><span id="member_totals"><?php echo count($_SESSION['valuePost']['member_child']) + 1 ?></span>
                        </div>
                </div>
                <div id="member_child">
                    <?php foreach($_SESSION['valuePost']['member_child'] as $key => $value){ ?>
                        <div class='sub_member' id='member_<?php $key ?>'>
                            <input type='button' class='remove' id='remove_<?php $key ?>' value='x' onclick='removeRow(this)' <?php echo $key == 2 ? "style='display: none'" : "style='display: block;z-index:9999'"  ?> >
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name</label>
                                    <input type="text" class="form-control" id="inputEmail4" value="<?php echo $value['fist_name'] ?>" name="member_child[<?php echo $key ?>][fist_name]">
                                    <label  class="error"><?php echo isset($arrError[$key]["first_name_required"]) ? $arrError[$key]["first_name_required"] : '' ?></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Second Name</label>
                                    <input type="text" class="form-control" id="inputPassword4" value="<?php echo $value['second_name'] ?>" name="member_child[<?php echo $key ?>][second_name]">
                                    <label  class="error"><?php echo isset($arrError[$key]["second_name_required"]) ? $arrError[$key]["second_name_required"] : '' ?></label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label class="form-check-label" for="gridRadios1">
                                        Age
                                    </label>
                                </div>
                                <div class="form-group col-md-5">
                                    <input type="range" class="form-control-range" id="range_slider_<?php echo $key ?>" name="member_child[<?php echo $key ?>][age]"  min="1" max="100" value="<?php echo $value['age'] ?>" oninput="setRangeValue(<?php echo $key ?>)">
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="form-check-label">
                                        Value : <span id="range_value_<?php echo $key ?>"><?php echo $value['age'] ?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            <?php }else{ ?>
                <div class="col-md-12">
                        <div id="add_member" class="form-wrapper form-group" style="display: none;">
                            <button type="button" onclick="OnlickAddButton()" class="btn btn-primary"> + Add Member</button>
                            <span>Member toals: </span><span id="member_totals">2</span>
                        </div>
                </div>
                <div id="member_child" style="display: none;">
                    <div class='sub_member' id='member_2'>
                        <input type='button' class='remove' id='remove_2' value='x' onclick='removeRow(this)' style="display:none">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">First Name</label>
                                <input type="text" class="form-control" id="inputEmail4" name="member_child[2][fist_name]">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Second Name</label>
                                <input type="text" class="form-control" id="inputPassword4" name="member_child[2][second_name]">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label class="form-check-label" for="gridRadios1">
                                    Age
                                </label>
                            </div>
                            <div class="form-group col-md-5">
                                <input type="range" class="form-control-range" id="range_slider_2" name="member_child[2][age]"  min="1" max="100" value="16" oninput="setRangeValue(2)">
                            </div>
                            <div class="form-group col-md-5">
                                <label class="form-check-label">
                                    Value : <span id="range_value_2">16</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Address</label>
            <textarea class="form-control" placeholder="Write something.." style="height:170px" name="address"><?php echo isset($_SESSION['valuePost']['address']) && $_SESSION['valuePost']['address'] ? $_SESSION['valuePost']['address'] : '' ?></textarea>
            <label  class="error"><?php echo isset($arrError["address_required"]) ? $arrError["address_required"] : ''  ?></label>
        </div>
        <div class="form-group group-money-total">
            <p>amount of money:
              <span> $</span><span id="money_total"><?php echo  isset($_SESSION['valuePost']['money_total']) && $_SESSION['valuePost']['money_total'] ? $_SESSION['valuePost']['money_total'] : "0.00" ?></span><input type="hidden" name="money_total" id="money_total_input" value="<?php echo isset($_SESSION['valuePost']['money_total']) && $_SESSION['valuePost']['money_total'] != '' ? $_SESSION['valuePost']['money_total'] : "0.00" ?>"/></p>
        </div>
        <input type="submit" class="btn" name="submit" value="Submit">
    </form>
</div>
<script>
    // caculate amount of money for all element is default
    var money_total = <?php  echo isset($_SESSION['valuePost']['money_total']) ? $_SESSION['valuePost']['money_total'] : 0; ?>;
    if (money_total == 0) {
        document.getElementById("money_total").innerHTML = 50 - checkRef(50, 'Yes') - checkAge(50, 16);
        document.getElementById("money_total_input").value = 50 - checkRef(50, 'Yes') - checkAge(50, 16);
    }

var member_totals = document.getElementById("member_totals");

// set init value and value when change scroll to element [value] in the right
function setRangeValue(i) {
    console.log(i);
   var range_slider = document.getElementById("range_slider_" + i);
   var age_value = document.getElementById("range_value_" + i);
   // set init range element [value] is default value  = 16
   age_value.innerHTML = range_slider.value;
   // set range element [value] when change scroll
   range_slider.oninput = function () {
      age_value.innerHTML = range_slider.value;
   }
}

function checkAge(cost, age) {
   var paymentViaAge = 0;
   if (16 <= age && age <= 19) {
      paymentViaAge = cost / 100 * 10
   } else {
      paymentViaAge = 0;
   }
   return paymentViaAge;
}

function checkRef(cost, ref_type) {
   var paymentViaRef = 0;
   if (ref_type == 'Yes') {
      paymentViaRef = cost / 100 * 5
   } else {
      paymentViaRef = 0;
   }
   return paymentViaRef;
}

function discount2_5() {
   return 40 / 100 * 2.5;
}


//event onchang when change value selectbox MembesShip Type 
function onChnageMeberType() {
}
//event onchang when change value selectbox Dualration Of Membership  
function onChnageMeberDuration() { }

function mountTotals() {
   var add_member = document.getElementById("add_member");
   var member_type = document.getElementById("member_type").value;
   var member_dualration = member_duaration = document.getElementById("member_duaration").value;
   var ref_type = document.querySelector('input[name="referral"]:checked').value;
   var age = document.getElementById('range_slider_1').value;
   console.log(member_type + '---' + member_dualration + '---' + ref_type + '---' + age);
   var payment_money = 0;

   if (member_type == "Family") {
      var member_count = parseInt(member_totals.innerText);
      if (member_count == 2) {
         payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age)) * member_dualration + (40 - checkAge(40, document.getElementById('range_slider_2').value)) * member_dualration;
      } else if (2 < member_count <= 5) {
         //caculate money for the first member
         payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age) - discount2_5()) * member_dualration
         //caculate money for the member 2 to 5
         for (let i = 2; i <= member_count; i++) {
            payment_money += (40 - checkAge(40, document.getElementById("range_slider_" + i + "").value) - discount2_5()) * member_dualration;
         }
      }
      if (member_count > 5) {
         //caculate money for the first member
         payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age) - discount2_5()) * member_dualration
         //caculate money for the member 2 to 5
         for (let i = 2; i <= 5; i++) {
            payment_money += (40 - checkAge(40, document.getElementById("range_slider_" + i + "").value) - discount2_5()) * member_dualration;
         }
         //caculate money for the member 6 to up
         for (let i = 6; i <= member_count; i++) {
            payment_money += 40;
         }
      }
      //display [add member] button 
      add_member.style.display = "block";
      // displays child member container 
      document.getElementById('member_child').style.display = 'block';
   } else {
      //hide [add member] button 
      add_member.style.display = "none";
      // hide child member container 
      document.getElementById('member_child').style.display = 'none';

      payment_money = (50 - checkRef(50, ref_type) - checkAge(50, age)) * member_dualration;
   }

   document.getElementById("money_total").innerHTML = payment_money;
   document.getElementById("money_total_input").value = payment_money;
}



// event onclick when  click to button [add member]
function OnlickAddButton() {
   var i = parseInt(member_totals.innerText) + 1;
   member_totals.innerHTML = i;
   // append add member elements when click to button [add member] 
   var str = "";
        str += " <div class='sub_member' id='member_"+i+"'> ";
        str +=" <div class='form-row'> ";
        str +=" <div class='form-group col-md-6'> ";
        str +=" <label for='inputEmail4'>First Name</label> ";
        str +="<input type='text' class='form-control' name='member_child["+i+"][fist_name]'> ";
        str +=" </div> ";
        str +=" <div class='form-group col-md-6'> ";
        str +=" <label >Second Name</label> ";
        str +="<input type='text' class='form-control' name='member_child["+i+"][second_name]'> ";
        str +=" </div>";
        str +="</div>";
        str +=" <div class='form-row'>";
        str +="<div class='form-group col-md-2'>";
        str +="<label class='form-check-label'>";
        str +=" Age";
        str +="</label>";
        str +="</div>";
        str +="<div class='form-group col-md-5'>";
        str +="<input type='range' class='form-control-range' name='member_child["+i+"][age]' id='range_slider_"+i+"' min='1' max='100' value='16' oninput='setRangeValue("+i+")'>";
        str +=" </div>";
        str +=" <div class='form-group col-md-5'>";
        str +=" <label class='form-check-label'>";
        str +="Value : <span id='range_value_"+i+"'>16</span>";
        str +=" </label>";
        str +="</div>";
        str +=" </div>";
        str +="</div>";
        str += "<input type='button' class='remove' id='remove_"+i+"' value='x' onclick='removeRow(this)'> ";

   document.querySelector('#member_child').insertAdjacentHTML('afterbegin', "<div class='sub_member' id='member_" + i + "'> "+str+" </div>");
   // hides previous child add member elements
   for (let index = 2; index <= i; index++) {
      var remove_button_hidden = document.getElementById("remove_" + index + "");
      console.log(remove_button_hidden);
      remove_button_hidden.style.display = "none";
   }
   // displays [x] on the last added child member
   show_x_button(parseInt(member_totals.innerText))
   mountTotals()
}

// remove submember member when click button [x]
function removeRow(input) {
    console.log("okkk");
   var i = parseInt(member_totals.innerText) - 1;
   console.log(input);
   member_totals.innerHTML = i;
   input.parentNode.remove();
   // displays [x] on the last added child member
   if (i > 2) {
      show_x_button(i)
   }
}


function show_x_button(index) {
   var remove_button_show = document.getElementById("remove_" + index + "");
   remove_button_show.style.display = "block";
}

// validate form after submit
function checkValidate() {
   var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   // var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
   var email = document.getElementById("email");
   var child_container = document.getElementById('member_child');
   // if child container hide only check validate for member_1  =>  i = 1
   var i = child_container.style.display == 'none' ? 1 : parseInt(member_totals.innerText);

   for (let index = 1; index <= i; index++) {
      var age = document.getElementById("range_slider_" + index);
      var first_name = document.getElementById("first_name_" + index);
      var second_name = document.getElementById("second_name_" + index);
      if (first_name.value.length == 0) {
         first_name.setCustomValidity("Please input your first name.");
      } else {
         first_name.setCustomValidity('');
      }
      if (second_name.value.length == 0) {
         second_name.setCustomValidity("Please input your second name.");
      } else {
         second_name.setCustomValidity('');
      }
      if (age.value < 16) {
         age.setCustomValidity('Please input age 16 and up.');
      } else {
         age.setCustomValidity('');
      }
   }
   if (email.value.length == 0) {
      email.setCustomValidity("Please input your e-maill.");
   } else if (!email.value.match(pattern)) {
      email.setCustomValidity('You have entered an invalid e-mail address.');
   } else {
      email.setCustomValidity('');
   }
}
//document.getElementsByName("submit")[0].onclick = checkValidate;
</script>