<?php
    require 'constants.php';
    //Get information when looking for my fitness
    $search = $_POST['name'];
    $res = array_filter(myfitness_work,function($value) use ($search){
        return strpos(strtolower($value['name']), strtolower($search)) !== false;
      },ARRAY_FILTER_USE_BOTH);
    echo json_encode($res);
?>