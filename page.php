<h1 class="title">Looking after my health today gives me a better hope for tomorrow</h1>
<div class="box-1">
    <p>Welcome to the ABC website!</p>
    <span>Melbourne-based Adrenaline Buzz Club (ABC) has been operating in brilliant Casey City for the past 20 years. 
        The club has been affected recently by the COVID epidemic, so we decided to build a website where the club members will continue activities and will be specifically supported as they are participate online</span>
</div>
<div class="box-2">
    <img src="lib/img/health1.jpg" alt="health" class="card-img">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-4 box-1">
            <span>At Adrenaline Buzz Club (ABC) our aim is to provide a welcoming and motivating environment,
                where all members of the community can join us on the journey towards a healthier and happier lifestyle.</span>
        </div>
        <div class="col-8">
            <img src="lib/img/health.jpg" alt="" class="img-fluid">
        </div>
    </div>
</div>
<div class="box-1">
    <p>At ABC...we have FUN!</p>
    <span>As our catch phrase states, we are not just your local gym in town who has a few treadmills, machines and dumbbells, we are a Club who always aim to make every single member equally important and want everyone to feel as if they're part of one big family! 
        Along with having such a great vibe in our clubs, our aim is to make exercise as enjoyable as possible, hence why you will see our Adrenaline Buzz Club (ABC) strutting his stuff around the place simply having FUN!</span>
</div>