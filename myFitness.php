<?php
  // No login will be redirected to the home page
  if(!isset($_SESSION['USER'])) {
    header('Location: '.'index.php?page=login');
  }
  unset($_SESSION['alert_message_success']);
  if (isset($_SESSION['USER']->login_success)) {
    $_SESSION['alert_message_success'] = msg_login_success;
    unset($_SESSION['USER']->login_success);
  }
?>
<div class="container myfitness">
    <h1 class="title">My Fitness</h1>
    <div class="row">
        <div class="col-md-8">
          <input class="form-control mr-md-2 name-search" style="width:25em;" type="text" placeholder="Search">
          <!-- <button class="btn btn-primary my-5 my-sm-0 search-value" type="submit">Search</button> -->
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-12">
            <p>Choose from any of the following category of exercies : </p>
        </div>
        <div class="col-md-12">
          <?php require 'alert-message.php'; ?>
        </div>
    </div>
    <div class="row list-work">
      <?php foreach(myfitness_work as $key => $value){ ?>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img src="lib/img/<?php echo $value['url_img'] ?>" alt="">
            <div class="card-body">
              <a href="index.php?page=detail-myfitness&id=<?php echo $value['id'] ?>" class="card-text"><?php echo $value['name'] ?></a>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
</div>

<script>
  $(document).ready(function(){
    $('.name-search').keyup(function(e){
      if (e.which == 13) {
        $.ajax({
          url:'call-ajax.php',
          type:'POST',
          dataType: 'json',
          data : {
            name : $(this).val()
          }
        }).done(function(res){
            var str = '';
            $('.list-work').html('');
            $.each(res,function(index,value){
              str += '<div class="col-md-4">';
              str += '<div class="card mb-4 shadow-sm">';
              str += '<img src="lib/img/'+value['url_img']+'" alt="">';
              str += '<div class="card-body">';
              str += '<a href="index.php?page=detail-myfitness&id='+value['id']+'" class="card-text">'+value['name']+'</a>';
              str += '</div>';
              str += '</div>';
              str += '</div>';
            });
            $('.list-work').append(str);
        });
      }
    });
  });
</script>